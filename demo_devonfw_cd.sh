
oc login -u developer -p dev

oc project dev

# Create QA and PROD environments by pointing to the tags for not-yet-created image stream

oc new-app entertainmentapp:qa --name entertainmentapp-qa --allow-missing-imagestream-tags=true

# oc new-app command does not create a service or route, addding these manually

oc set triggers dc/entertainmentapp-qa --manual
oc expose dc/entertainmentapp-qa --port=8080
oc expose svc entertainmentapp-qa

oc new-app entertainmentapp:prod --name entertainmentapp-prod --allow-missing-imagestream-tags=true

oc set triggers dc/entertainmentapp-prod --manual
oc expose dc/entertainmentapp-prod --port=8080
oc expose svc entertainmentapp-prod

#
# Pipelines deployment
#
oc project jenkins

# Creates the pipelines
oc new-app https://dgauda@bitbucket.org/dgauda/entertainment-simulator#master --context-dir=cicd/pipelines/cd --name cd-pipeline