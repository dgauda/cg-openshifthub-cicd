

# Create project to store build base-images
oc new-project devonfw --display-name='DevonFW' --description='DevonFW'

# Create base-images and add them to DevonFW project
oc create -f https://bitbucket.org/dgauda/devonfw-shop-floor/raw/ef77ed723d08da88b9a37b212de4d610632876e8/dsf4openshift/openshift-devonfw-deployment/s2i/java/s2i-devonfw-java-imagestream.json --namespace=devonfw

# Build base-image in DevonFW project
oc start-build s2i-devonfw-java --namespace=devonfw

# Setup the DevonFW project as "image-puller" to be used in other projects in the same cluster
oc policy add-role-to-group system:image-puller system:authenticated --namespace=devonfw

oc login -u system:admin

# Create DevonFW templates into openshift
oc create -f https://bitbucket.org/dgauda/devonfw-shop-floor/raw/ef77ed723d08da88b9a37b212de4d610632876e8/dsf4openshift/openshift-devonfw-deployment/templates/devonfw-java-template.json --namespace=openshift

oc policy add-role-to-user cluster-developer developer
oc policy add-role-to-user developer developer

oc policy add-role-to-user developer system:serviceaccount -n openshift
oc policy add-role-to-user edit system:serviceaccount -n openshift

oc login -u developer -p dev

# Jenkins
oc new-project jenkins --display-name=JENKINS
oc new-app --template=jenkins-persistent -p MEMORY_LIMIT=1Gi -l app=jenkins

# Environment Creation
oc new-project dev --display-name=DEV

# Grant edit access to developer in dev project
oc adm policy add-role-to-user edit developer -n dev

# Grant view access to developer in jenkins project
oc adm policy add-role-to-user edit developer -n jenkins

# Grant edit access to jenkins service account
oc policy add-role-to-user edit system:serviceaccount:jenkins:jenkins -n dev

# Create the application definition for your application using oc process from your template created above
# Replace the parameters accordingly
oc process openshift//devonfw-java -p APPLICATION_NAME=parkingapp -p APPLICATION_GROUP_NAME=Parking -p GIT_URI=https://dgauda@bitbucket.org/dgauda/parking-simulator.git -p GIT_REF=master -p CONTEXT_DIR= >parking.json

# Turn automatic image triggers off and set the initial number of replicas to 0

sed '1,/\"automatic\": true/s/\"automatic\": true/\"automatic\": false/' parking.json >parking-nobuild.json

sed '1,/\"replicas\": 1/s/\"replicas\": 1/\"replicas\": 0/' parking-nobuild.json >parking-zero.json
   
oc project dev

# create the application using newly created json file
oc create -f parking-zero.json

#
# Pipelines deployment
#
oc project jenkins

# Creates the pipelines
oc new-app https://dgauda@bitbucket.org/dgauda/parking-simulator#master --context-dir=cicd/pipelines/ci --name ci-pipeline 

